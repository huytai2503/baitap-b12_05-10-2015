package khtn.main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import khtn.edu.DsSVien;
import khtn.edu.SinhVien;

public class Main {
	static Scanner	sc		= new Scanner(System.in);
	static String	path	= "DsSVien.txt";
	static String	path2	= "DsSVien2.txt";

	public static void main(String[] args) {
		batDau();
		sc.close();
	}

	public static void batDau() {
		System.out.println("		<< Màn hình chính >>");
		DsSVien dssv = new DsSVien();
		String sChon = "";
		int chon = 0;
		try {
			dssv = docObjectTuFile();
		} catch (NullPointerException e) {
			System.out.println("Danh sách trống");
			System.out.println();
		}
		do {
			System.out.println("Thêm SV(1), Xóa SV(2), Xem DS(3), Thoát(0)");
			System.out.print("Chọn: ");
			sChon = sc.nextLine();
			System.out.println("==================================");
			if (isNumber(sChon))
				chon = Integer.parseInt(sChon);
			else
				batDau();
			if (chon > 3)
				System.out.println("Hãy chọn trong danh mục");
			else {
				switch (chon) {
					case 1:
						chon = 0;
						themSV(dssv);
						break;
					case 2:
						chon = 0;
						xoaSV(dssv);
						break;
					case 3:
						if (dssv.getDssv().isEmpty()) {
							chon = 0;
							xemDS(dssv);
						} else {
							chon = 0;
							xemDS(dssv);
							batDau();
						}
						break;
					case 0:
						System.out.println("Kết thúc! cảm ơn đã sử dụng.");
						break;
					default:
						break;
				}
			}
		} while (chon > 3);
		chon = 0;
	}

	public static void xemDS(DsSVien dssv) {
		if (dssv.getDssv().isEmpty()) {
			System.out.println("Danh sách trống");
			System.out.println("==================================");
			batDau();
		} else {
			int count = 1;
			System.out.println("Danh sách hiện có:");
			for (SinhVien sv : dssv.getDssv()) {
				System.out.print((count++) + ":");
				System.out.print(sv + "\n");
			}
			System.out.println("==================================");
		}
	}

	public static void themSV(DsSVien dssv) {
		System.out.println("<< Màn hình thêm SV >>");
		SinhVien sv = new SinhVien();
		String sChon = "";
		int chon = 0;
		do {
			System.out.print("Nhập tên Sinh Viên: ");
			sv.setTen(sc.nextLine());
			System.out.print("Nhập địa chỉ: ");
			sv.setDiachi(sc.nextLine());
			System.out.println("==================================");
			try {
				dssv = docObjectTuFile();
			} catch (NullPointerException e) {
			}
			dssv.addSV(sv);
			ghiObjectRaFile(dssv);
			do {
				System.out.println("Thêm SV(1), Quay về(2)");
				System.out.print("Chọn: ");
				sChon = sc.nextLine();
				if (isNumber(sChon))
					chon = Integer.parseInt(sChon);
				if (chon != 1 && chon != 2)
					System.out.println("Hãy chọn trong danh mục");
			} while (chon != 1 && chon != 2);
		} while (chon == 1);
		System.out.println("==================================");
		batDau();
	}

	public static void xoaSV(DsSVien dssv) {
		System.out.println("<< Màn hình xóa SV >>");
		String sChon = "";
		int chon = 0;
		if (dssv.getDssv().isEmpty()) {
			System.out.println("Danh sách trống");
			System.out.println("==================================");
			batDau();
		} else {
			do {
				do {
					xemDS(dssv);
					System.out.print("Quay về(0), hoặc chọn SV thứ mấy muốn xóa: ");
					sChon = sc.nextLine();
					if (isNumber(sChon))
						chon = Integer.parseInt(sChon);
					if (chon < 0 || chon > dssv.size())
						System.out.println("Không có SV thứ: "+ sChon);
				} while (chon < 0 || chon > dssv.size());
				if(chon!=0){
					System.out.println("Đã xóa: "+ dssv.getDssv().get(chon - 1));
					System.out.println("==================================");
					dssv.getDssv().remove(chon - 1);
					ghiObjectRaFile(dssv);
					chon = 0;
					if (dssv.getDssv().isEmpty())
						System.out.println("Danh sách trống");
					else {
						do {
							System.out.println("Xóa tiếp(1), Quay về(2)");
							System.out.print("Chọn: ");
							sChon = sc.nextLine();
							if (isNumber(sChon))
								chon = Integer.parseInt(sChon);
							if (chon != 1 && chon != 2)
								System.out
										.println("Hãy chọn trong danh mục");
						} while (chon != 1 && chon != 2);
					}
				}
			} while (chon == 1);
			chon = 0;
			System.out.println("==================================");
			batDau();
		}
	}

	public static boolean isNumber(String number) {
		try {
			Integer.parseInt(number);
		} catch (NumberFormatException e) {
			System.out.println("Lỗi! hãy nhập số");
			System.out.println("==================================");
			return false;
		}
		return true;
	}

	public static void ghiObjectRaFile(DsSVien dssv) {
		try {
			ObjectOutputStream objOut = new ObjectOutputStream(
					new BufferedOutputStream(
							new FileOutputStream(path)));
			objOut.writeObject(dssv);
			objOut.close();
		} catch (FileNotFoundException e) {
			System.out.println("Không tìm thấy file: "+ path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static DsSVien docObjectTuFile() {
		try {
			ObjectInputStream objIn = new ObjectInputStream
					(new BufferedInputStream(new FileInputStream(path)));
			DsSVien dssv = (DsSVien) objIn.readObject();
			objIn.close();
			return dssv;
		} catch (FileNotFoundException e) {
			System.out.println("Không tìm thấy file: "+ path);
			return null;
		} catch (IOException e) {
			System.out.println("Class đã bị thay đổi, không thể đọc file");
			return null;
		} catch (ClassNotFoundException e) {
			System.out.println("Không tìm thấy Class");
			return null;
		}
	}
	
//	Hàm đọc ghi file dùng luồng Reader and Writer để các bạn tham khảo.
//	Khi muốn ghi ra file, thì cần đưa vào hàm ghiStringRaFile dưới đây
//	một dssv đã tạo các sv bên trong.
//	Ví dụ:
//	DsSVien dssv = new DsSVien();
//	SinhVien sv = new SinhVien();
//	sv.setTen(sc.nextLine());
//	sv.setDiaChi(sc.nextLine());
//	dssv.addSV(sv);
//	ghiStringRaFile(dssv);
//	
//	public static void ghiStringRaFile(DsSVien dssv) {
//		try {
//			BufferedWriter objOut = new BufferedWriter
//					(new OutputStreamWriter(new FileOutputStream(path2),"UTF8"));
//			for (SinhVien sv : dssv.getDssv()) {
//				objOut.write(sv.getTen() + "-");
//				objOut.write(sv.getDiachi() + ",");
//			}
//			objOut.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

//	Muốn đọc dssv từ file, cần tạo một đối tượng dssv mới, sau đó gán hàm
//	docStringTuFile dưới đây cho dssv mới tạo.
//	Ví dụ:
//	DsSVien dssv = new DsSVien();
//	dssv = docStringTuFile();
//	
//	public static DsSVien docStringTuFile() {
//		try {
//			BufferedReader objIn = new BufferedReader
//					(new InputStreamReader(new FileInputStream(path2),"UTF-8"));
//			DsSVien dssv = new DsSVien();
//			String[] arrSV = objIn.readLine().split(",");
//			String[] arrChiTiet = new String[arrSV.length];
//			for (int i = 0; i < arrSV.length; i++) {
//				arrChiTiet = arrSV[i].split("-");
//				SinhVien sv = new SinhVien(arrChiTiet[0],arrChiTiet[1]);
//				dssv.addSV(sv);
//			}
//			objIn.close();
//			return dssv;
//		} catch (FileNotFoundException e) {
//			System.out.println("Không tìm thấy file: "+ path2);
//		} catch (IOException e) {
//			System.out.println("Class bị thay đổi, không thể đọc file");
//		}
//		return null;
//	}
}
