package khtn.edu;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SinhVien implements Serializable{
	String ten,diachi;
	public SinhVien(String ten, String diachi) {
		super();
		this.ten = ten;
		this.diachi = diachi;
	}
	public SinhVien() {
		super();
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getDiachi() {
		return diachi;
	}
	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}
	@Override
	public String toString() {
		return "(" + ten + " - "
				+ diachi + ")";
	}
}
