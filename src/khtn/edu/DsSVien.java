package khtn.edu;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class DsSVien implements Serializable{
	ArrayList<SinhVien> dssv = new ArrayList<SinhVien>();
	public DsSVien(ArrayList<SinhVien> dssv) {
		super();
		this.dssv = dssv;
	}
	public DsSVien() {
		super();
	}
	public ArrayList<SinhVien> getDssv() {
		return dssv;
	}
	public void setDssv(ArrayList<SinhVien> dssv) {
		this.dssv = dssv;
	}
	@Override
	public String toString() {
		return "DsSVien:" + dssv;
	}
	public void addSV(SinhVien sv) {
		dssv.add(sv);
	}
	public int size() {
		return dssv.size();
	}
}
